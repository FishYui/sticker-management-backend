import { Injectable } from '@nestjs/common';
import * as dotenv from 'dotenv';
import * as fs from 'fs';
import * as path from 'path';

@Injectable()
export class ConfigService {
  private envConfig;

  constructor() {
    const envFileName = `${process.env.NODE_ENV || 'development'}.env`;
    const envFilePath = path.resolve(
      __dirname,
      '../../../config/',
      envFileName
    );
    this.envConfig = dotenv.parse(fs.readFileSync(envFilePath));
  }

  get(key) {
    return this.envConfig[key];
  }
}
