import { DynamicModule, Module } from '@nestjs/common';
import { MINIO_CONFIG_OPTIONS } from './constants';
import { IMinioConnectionAsyncOptions } from './interfaces';
import { MinioService } from './minio-client.service';

@Module({})
export class MinioModule {
  static registerAsync(options: IMinioConnectionAsyncOptions): DynamicModule {
    return {
      module: MinioModule,
      imports: options.imports,
      providers: [
        {
          provide: MINIO_CONFIG_OPTIONS,
          useFactory: options.useFactory,
          inject: options.inject
        },
        MinioService
      ]
    };
  }
}
