import { Inject, Injectable } from '@nestjs/common';
import * as Minio from 'minio';
import { ConfigService } from '../config/config.service';
import { BufferFile } from '../../types';
import { MINIO_CONFIG_OPTIONS } from './constants';

@Injectable()
export class MinioService {
  private readonly minioClient: Minio.Client;
  private readonly baseBucketName: string;

  constructor(
    @Inject(MINIO_CONFIG_OPTIONS)
    private readonly options: Minio.ClientOptions,
    private readonly configService: ConfigService
  ) {
    this.baseBucketName = configService.get('MINIO_BUCKET_NAME');
    this.minioClient = new Minio.Client(options);
  }

  public client() {
    return this.minioClient;
  }

  public async upload(
    file: BufferFile,
    fileName: string,
    bucketName: string = this.baseBucketName
  ) {
    const metaData = {
      'Content-Type': file.mimetype
    };

    return await this.minioClient.putObject(
      bucketName,
      fileName,
      file.buffer,
      metaData
    );
  }

  public async get(fileName: string, bucketName: string = this.baseBucketName) {
    return await this.minioClient.getObject(bucketName, fileName);
  }

  public async remove(
    fileName: string,
    bucketName: string = this.baseBucketName
  ) {
    return await this.minioClient.removeObject(bucketName, fileName);
  }
}
