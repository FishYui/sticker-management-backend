import * as Minio from 'minio';

export interface IMinioConnectionAsyncOptions {
  imports: any[];
  useFactory: (
    ...arg: any[]
  ) => Promise<Minio.ClientOptions> | Minio.ClientOptions;
  inject?: any[];
}
