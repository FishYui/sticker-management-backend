import { Module } from '@nestjs/common';
import { ConfigModule } from './modules/config/config.module';
import { ConfigService } from './modules/config/config.service';
import { MinioModule } from './modules/minio-client/minio-client.module';

@Module({
  imports: [
    ConfigModule,
    MinioModule.registerAsync({
      imports: [ConfigModule],
      useFactory: async (configService: ConfigService) => ({
        endPoint: configService.get('MINIO_END_POINT'),
        port: parseInt(configService.get('MINIO_PORT')),
        useSSL: configService.get('MINIO_USE_SSL') === 'false' ? false : true,
        accessKey: configService.get('MINIO_ACCESS_KEY'),
        secretKey: configService.get('MINIO_SECRET_KEY')
      }),
      inject: [ConfigService]
    })
  ]
})
export class AppModule {}
